package com.formaciondbi.springboot.app.usuarios.models.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import com.formaciondbi.springboot.app.usuarios.commons.models.entity.Usuario;




@RepositoryRestResource(path = "usuarios")
public interface UsuarioRepository extends PagingAndSortingRepository<Usuario, Long>{
	
	@RestResource( path = "buscarByUserName")
	public Usuario findByUsername( @Param("username	")String username );
	
	
	
	@RestResource( path = "buscarPorUserName")
	@Query("Select u from Usuario u where u.username= :username")
	public Usuario buscarPorUserName( @Param("username") String username );
	
}
