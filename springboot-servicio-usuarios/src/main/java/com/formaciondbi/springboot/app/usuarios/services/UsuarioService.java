package com.formaciondbi.springboot.app.usuarios.services;

import java.util.List;

import com.formaciondbi.springboot.app.usuarios.commons.models.entity.Usuario;





public interface UsuarioService {
	
	List<Usuario> getAllUsuarios();

}
