package com.formaciondbi.springboot.app.usuarios.commons.models.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name="usuarios")
public class Usuario implements Serializable{
	
	@Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Long id;
	
	private Boolean enabled;
    
	@Column( length = 60)
	private String password;
    
    @Column( unique = true, length = 20 )
    private String username;
    
    private String nombre;
    private String apellido;
    
    @Column( unique = true, length = 100 )
    private String email;
    
    
    @ManyToMany(fetch = FetchType.LAZY  )
    @JoinTable(name = "USUARIOS_ROLES",
            joinColumns = { @JoinColumn(name = "USUARIO_ID") },
            inverseJoinColumns = { @JoinColumn(name = "ROLE_ID") },
            uniqueConstraints = { @UniqueConstraint(columnNames = { "USUARIO_ID", "ROLE_ID" } ) } )
    private Set<Role> roles = new HashSet<>(); 
    
    
    
	public Long getId() {
		return id;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public String getPassword() {
		return password;
	}

	public String getUsername() {
		return username;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getNombre() {
		return nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public String getEmail() {
		return email;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public void setEmail(String email) {
		this.email = email;
	}

    
	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}




	private static final long serialVersionUID = 1L;

}
