package com.formacionbdi.springboot.app.productos.controllers;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
//import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.formaciondbi.springboot.app.commons.models.entity.Producto;
import com.formacionbdi.springboot.app.productos.models.service.IProductoService;
import com.netflix.ribbon.proxy.annotation.Http;

@RestController
public class ProductoController {
	
	//@Autowired
	//private Environment env;
	
	@Value("${server.port}")
	private Integer port;
	
	@Autowired
	private IProductoService productoService;
	
	@GetMapping("/listar")
	public List<Producto> listar(){
		return productoService.findAll().stream().map(producto ->{
			//producto.setPort(Integer.parseInt(env.getProperty("local.server.port")));
			producto.setPort(port);
			return producto;
		}).collect(Collectors.toList());
	}
	
	@GetMapping("/ver/{id}")
	public Producto detalle(@PathVariable Long id) {
		Producto producto = productoService.findById(id);
		//producto.setPort(Integer.parseInt(env.getProperty("local.server.port")));
		producto.setPort(port);
		
		/*
		 * try {
			Thread.sleep(2000L);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		
		return producto;
	}
	
	/*@RequestMapping( value = "/", method = RequestMethod.POST )
	public ResponseEntity<?> saveProducto( @RequestBody Producto producto ){
		Producto newProducto = productoService.save(producto);
		return new ResponseEntity<>( newProducto, HttpStatus.OK );
	}*/
	
	@PostMapping(  "/" )
	@ResponseStatus( HttpStatus.CREATED )
	public Producto saveProducto( @RequestBody Producto producto ){
		Producto newProducto = productoService.save(producto);
		return newProducto;
	}
	
	/*
	  @RequestMapping( value="/{id}", method = RequestMethod.PUT)
	  public ResponseEntity<?> updateProducto( @PathVariable Long id, @RequestBody Producto producto ){
			Producto updatedProducto = productoService.update(id, producto);
			return new ResponseEntity<>( updatedProducto, HttpStatus.OK  );
	    } */
	
	
	@PutMapping( "/{id}" )
	@ResponseStatus( HttpStatus.CREATED )
	public Producto updateProducto( @PathVariable Long id, @RequestBody Producto producto ){
		Producto updatedProducto = productoService.update(id, producto);
		return  updatedProducto;
	}
	
	
	
	/*@RequestMapping( value="/{id}", method = RequestMethod.DELETE )
	public ResponseEntity<?> deleteProducto( @PathVariable Long id ){		
		Producto deletedProducto = productoService.findById(id);
		productoService.delete(id);
		return new ResponseEntity<>(deletedProducto, HttpStatus.OK );
	}*/
	
	@DeleteMapping( "/{id}" )
	@ResponseStatus( HttpStatus.NO_CONTENT )
	public Producto deleteProducto( @PathVariable Long id ){		
		Producto deletedProducto = productoService.findById(id);
		productoService.delete(id);
		return deletedProducto;
	}

}
