package com.formacionbdi.springboot.app.productos.models.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.formacionbdi.springboot.app.productos.models.dao.ProductoDao;
import com.formaciondbi.springboot.app.commons.models.entity.Producto;


@Service
public class ProductoServiceImpl implements IProductoService{

	@Autowired
	private ProductoDao productoDao;
	
	@Override
	@Transactional(readOnly = true)
	public List<Producto> findAll() {
		return (List<Producto>) productoDao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Producto findById(Long id) {
		return productoDao.findById(id).orElse(null);
	}

	@Override
	@Transactional(readOnly = false)
	public Producto save( Producto producto ) {		
		Producto newProducto = productoDao.save( producto );
		return newProducto;
	}

	@Override
	@Transactional(readOnly = false)
	public Producto delete(Long id) {		
		Producto deletedProducto = findById( id );		
		productoDao.deleteById(id);
		return deletedProducto;
	}

	@Override
	@Transactional(readOnly = false)
	public Producto update(Long id, Producto producto) {			
		Producto foundedProducto = findById( id );
		//foundedProducto.setCreateAt(createAt);
		foundedProducto.setNombre( producto.getNombre() );
		foundedProducto.setPrecio( producto.getPrecio() );
		Producto updatedProducto = productoDao.save( foundedProducto ); 
		return updatedProducto;	
	}	

}
