package com.formaciondbi.springboot.app.oauth;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringbootServicioOauthApplicationTests {
	
	@Autowired
	BCryptPasswordEncoder encoder;


	@Test
	public void contextLoads() {
		System.out.println( "**" + encoder.encode("12345") + "**" );
	}

}
