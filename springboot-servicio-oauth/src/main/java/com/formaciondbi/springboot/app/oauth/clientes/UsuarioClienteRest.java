package com.formaciondbi.springboot.app.oauth.clientes;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.formaciondbi.springboot.app.usuarios.commons.models.entity.Usuario;

@FeignClient(name="servicio-usuarios")
public interface UsuarioClienteRest {
	
	//http://localhost:8090/api/usuarios/usuarios/search/buscarPorUserName?username=admin
		
	@GetMapping("/usuarios/search/buscarPorUserName")
	Usuario buscarPorUserName( @RequestParam String username); 

}
