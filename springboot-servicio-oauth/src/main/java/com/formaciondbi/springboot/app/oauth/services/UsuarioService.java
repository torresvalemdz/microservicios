package com.formaciondbi.springboot.app.oauth.services;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.formaciondbi.springboot.app.oauth.clientes.UsuarioClienteRest;
import com.formaciondbi.springboot.app.usuarios.commons.models.entity.Usuario;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Service
public class UsuarioService implements UserDetailsService {
	
	private Logger log = LoggerFactory.getLogger(UsuarioService.class);
	
	@Autowired
	UsuarioClienteRest usuarioClienteRest;
	

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		Usuario usuario = usuarioClienteRest.buscarPorUserName(username);
		
		if( usuario == null ) {
			log.error("Error en el login, no existe el usuario '" + username + "' en el sistema");
			throw new UsernameNotFoundException("Error en el login, no existe el usuario '" + username + "' en el sistema");
		}
		
		List< GrantedAuthority > authorities = 
				usuario.getRoles()
				       .stream() 	
					  .map( role -> new SimpleGrantedAuthority(  role.getNombre() ) )
					  .peek( authority -> log.info( "Role:" + authority.getAuthority() ))
					  .collect(Collectors.toList());		
				
		return new User( usuario.getUsername(), usuario.getPassword(), usuario.getEnabled(),
						 true, true, true, authorities);
	}

}
