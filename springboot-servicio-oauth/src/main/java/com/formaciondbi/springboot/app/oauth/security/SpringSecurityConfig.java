package com.formaciondbi.springboot.app.oauth.security;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter{

	/// Spring busca que clase implements UserDetailsService y lo inyecta en nuestro caso el 
	// caso concreto -> UsuarioService
	@Autowired
	UserDetailsService usuarioService;
	
	//Lo anotamos como @Bean para registrarlo en el contenedor de Spring y podamos utilizarlo.
	//Via metodo lo que retorna el metodo es lo que se registra
	@Bean 
	public BCryptPasswordEncoder passwordEncoder(){
		return new BCryptPasswordEncoder(); 
	}

	/// Lo anoto @Autowired para que pueda inyectar el auth
	@Override
	@Autowired
	protected void configure(AuthenticationManagerBuilder auth) throws Exception { 
		// Automaticamente cuando el usuario se loguea se encripta su contraseña en BCrypt y lo va a comparar con BD		
		
		auth.userDetailsService( this.usuarioService ).passwordEncoder(passwordEncoder());		
	}

	@Override
	@Bean
	protected AuthenticationManager authenticationManager() throws Exception { 
		return super.authenticationManager();
	}	
	
	
}
