package com.formacionbdi.springboot.app.item.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.formacionbdi.springboot.app.item.models.Item;
import com.formaciondbi.springboot.app.commons.models.entity.Producto;
import com.formacionbdi.springboot.app.item.models.service.ItemService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.ribbon.proxy.annotation.Http.HttpMethod;

@RefreshScope
@RestController
public class ItemController {
	
	private static Logger log = LoggerFactory.getLogger(ItemController.class);
	
	@Autowired
	Environment env;
	
	/*@Autowired
	@Qualifier("serviceFeign")
	private ItemService itemService;*/

	@Autowired
	@Qualifier("serviceRestTemplate")
	private ItemService itemService;
	
		
	@Value("${configuracion.texto}")
	private String configTexto;
	
	
	@GetMapping("/listar")
	public List<Item> listar(){
		return itemService.findAll();
	}
	
	@HystrixCommand(fallbackMethod = "metodoAlternativo")
	@GetMapping("/ver/{id}/cantidad/{cantidad}")
	public Item detalle(@PathVariable Long id, @PathVariable Integer cantidad) {
		return itemService.findById(id, cantidad);
	}
	
	
	@PostMapping( "/crearProducto" )
	@ResponseStatus( HttpStatus.CREATED )
	public Producto saveProducto( @RequestBody Producto producto) {
		return itemService.save(producto);
	}
	
	@DeleteMapping( "/borrarProducto/{id}" )
	@ResponseStatus( HttpStatus.NO_CONTENT )
	public void deleteItem( @PathVariable Long id) {		
		 itemService.delete(id); 
	}
	
	
	@PutMapping( "/modificarProducto/{id}" )
	@ResponseStatus( HttpStatus.CREATED)
	public Producto updatedItem( @PathVariable Long id, @RequestBody Producto producto) {
		return itemService.update(id, producto);		
	}

	public Item metodoAlternativo(Long id, Integer cantidad) {
		Item item = new Item();
		Producto producto = new Producto();
		
		item.setCantidad(cantidad);
		producto.setId(id);
		producto.setNombre("Camara Sony");
		producto.setPrecio(500.00);
		item.setProducto(producto);
		return item;
		
	}
	
	@GetMapping("/config")
	public ResponseEntity<?> getConfig( @Value("${server.port}") Integer port,
										@Value("${configuracion.autor.nombre}") String nombre,
										@Value("${configuracion.autor.email}") String email ){	
		
		log.info(  configTexto );
		
		Map<String, String> config = new HashMap();
		config.put("server.port", String.valueOf( port ) );
		config.put("configuracion.texto", configTexto);
		
		if( env.getActiveProfiles().length > 0  
				&& env.getActiveProfiles()[0].equals("dev") ){
			config.put("configuracion.autor.nombre", nombre );			
			config.put("configuracion.autor.email", email);
		}
		
		return new ResponseEntity<>( config, HttpStatus.OK );
	}
	
}
