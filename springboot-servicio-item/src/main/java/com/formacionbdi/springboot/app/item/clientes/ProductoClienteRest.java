package com.formacionbdi.springboot.app.item.clientes;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.formaciondbi.springboot.app.commons.models.entity.Producto;

@FeignClient(name = "servicio-productos")
public interface ProductoClienteRest {
	
	@GetMapping("/listar")
	public List<Producto> listar();
	
	@GetMapping("/ver/{id}")
	public Producto detalle(@PathVariable Long id);
	
	@RequestMapping( value = "/", method = RequestMethod.POST )
	public Producto saveProducto( @RequestBody Producto producto );
	
	@RequestMapping( value="/{id}", method = RequestMethod.PUT)
	public Producto updateProducto( @PathVariable Long id, @RequestBody Producto producto );
	
	@RequestMapping( value="/{id}", method = RequestMethod.DELETE )
	public Producto deleteProducto( @PathVariable Long id );

}
