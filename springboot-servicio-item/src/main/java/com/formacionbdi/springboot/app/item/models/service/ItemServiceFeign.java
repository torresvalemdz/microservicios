package com.formacionbdi.springboot.app.item.models.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.formacionbdi.springboot.app.item.clientes.ProductoClienteRest;
import com.formacionbdi.springboot.app.item.models.Item;
import com.formaciondbi.springboot.app.commons.models.entity.Producto;

@Service("serviceFeign")
public class ItemServiceFeign implements ItemService {
	
	@Autowired
	private ProductoClienteRest clienteFeign;

	@Override
	public List<Item> findAll() {
		return clienteFeign.listar().stream().map(p -> new Item(p, 1)).collect(Collectors.toList());
	}

	@Override
	public Item findById(Long id, Integer cantidad) {
		return new Item(clienteFeign.detalle(id), cantidad);
	}

	@Override
	public Producto save(Producto producto ) {
        Producto newProducto = clienteFeign.saveProducto( producto ); 
		return newProducto;
	}

	@Override
	public void delete( Long id ) {
		Producto deletedProducto = clienteFeign.deleteProducto(id);
		//return null;
	}

	@Override
	public Producto update(Long id, Producto producto ) {
		Producto updated = clienteFeign.updateProducto(id, producto);
		return updated;
	}

}
